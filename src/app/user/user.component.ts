import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers: [ UserService, DataService ]
})
export class UserComponent implements OnInit {

  isLogged: boolean = false;
  user: { name: string };
  data: string = '';

  constructor(private userService: UserService, private dataService: DataService) { }

  ngOnInit() {
    this.user = this.userService.getUser();
    this.dataService.getUserDetails().then(
      (data: string) => {
        this.data = data;
      }
    );
  }

}
