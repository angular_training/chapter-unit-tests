import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserComponent } from './user.component';
import { UserService } from '../services/user.service';
import { DataService } from '../services/data.service';
import { log } from 'util';

describe('UserComponent Tests', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;
  let nativeEl: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.debugElement.componentInstance;
    nativeEl = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create user component', () => {
    expect(component).toBeTruthy();
  });

  it('should get user name from the service', () => {
    let userService = fixture.debugElement.injector.get(UserService);
    expect(component.user.name).toEqual(userService.getUser().name);
  });

  it('should display the user name if it\'s logged', () => {
    component.isLogged = true;
    fixture.detectChanges();
    expect(nativeEl.querySelector('div').textContent).toContain(component.user.name);
  });
  
  it('should not return user name if not logged', () => {
    // expect(nativeEl.querySelector('div').textContent.includes(component.user.name)).toBeFalsy();
    expect(nativeEl.querySelector('div').textContent).not.toContain(component.user.name);
  });

  it('should fetch data successfully when called asynchronously', async(() => {
    let dataService = fixture.debugElement.injector.get(DataService);
    let spy = spyOn(dataService, 'getUserDetails').and.returnValue(Promise.resolve('Data'));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.data).toBe('Data');
    }).catch(
      (error) => console.log(error)
    );
  }));
  
});
